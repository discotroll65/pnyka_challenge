# Pnyka Candidate Challenge - Intern Version

### Introduction
This challenge is meant to test your programming capabilities in relation to our company and our available position. This programming assignment is to be completed by you alone. You can consult the web and other sources for assistance, but the core code submitted must be your own.

In this challenge, we will replicate part of the Pnyka application. A critical part of the Pnyka app is placing participants in balanced assemblies (chat rooms) based on their response to varying topics. In this challenge we ask you to complete code that will place participants into balanced groups.

### Problem Statement
You will be given an example dataset of users. These users have all responded to 5 topic prompts with the following answers: “Yes”, “Unsure”, “No”. We ask you to place these users in assemblies for different topics with an ideal format of:
* 2 “Yes”
* 2 “Unsure”
* 2 “No”

You’re main goal is to place the maximum number of users into assemblies. Each user should only be placed into one assembly. Also, the users can all be placed in to different assemblies with the same topic, or they can be placed in to assemblies of differing topics. Again the ideal format is a 2/2/2 placement, however this a chance this might not be achievable given the participant data.

You are to create a script that provides a solution to the problem statement above. The script you write should be written in Node.js. The main part of your script should be in the `\src\assembly_placement.js` file. Also, feel free to include any utility functions, etc. that you copied from the web in the `\src\utils.js` script. (We want to be clear about what code is yours, and which code you borrowed from external sources).

Finally, your script should output a JSON structure. The JSON should be saved as `\assembly_placement.json`. The structure should include a list of assemblies. Each assembly should have the following properties:
* ` topic` | `string` - the definition of the topic statement that will be discussed in the assembly
* ` id` | `integer` - an identifier to distinguish the assemblies (can just be the index)
* `participants` | `[objects]`- a list of participants for this assembly, this list should include info about how they responded to the topic of the assembly that they are placed in (“Yes”, “Unsure”, “No”)

#### The Dataset

##### Participants

The example dataset is `\data\participants.csv`. This dataset includes a list of participants and how they responded to the topics. Each of their responses is in the column designated with that topic id + `response` - so for the first topic responses can be found in the column `T1_Response`.

This dataset includes additional data for each topic about how the participants responded to the following questions in regards to that topic:

* `T1_Viewpoint` - How well do you know the differing viewpoint(s) for the issue being discussed?

* `T1_Receptive` - How receptive do you feel to people who disagree with you on this prompt?

##### Topics

There is another dataset file called `\data\topics.csv` that includes the actual statement for each topic that the user responded to. The topic ids in this dataset map to the response columns of the `participant.csv` file.

### Rules

* All code in the `assembly_placement.js` file should be of your own design and ideas. You are allowed to copy code from the web, however copied could should only help to solve some sort of utility role (this code must be placed and referenced from the `utils.js` file).

* Your submission should be run-able from the command line. All required packages should be included in the `package.json` of your submission.

* Your submission will be run using the `npm start` command. (Currently this calls the `src/assembly_placement.js` as the main script.)

### Deliverables

* Please submit all of your source code and output files. Plese do not include any node package/libraries in your submission. We will run `npm install` before testing your code, so no need to include Node packages.

* A brief explanation of your approach to the solution in `/SOLUTION.md`.
<!--stackedit_data:
eyJoaXN0b3J5IjpbNjM5NDM3MzI3XX0=
-->