// Assembly placement algorithm class - all of the code for your solution will go in this class

const fs = require('fs');
const async = require("async");
const csv = require('fast-csv');
const uniqid = require('uniqid');
const jsonfile = require('jsonfile')

/*
The Assembly class will store the id (topic #), subject, participants, and a
breakdown of participants that voted yes, no, and unsure to the prompt
*/
function Assembly(id,topics){
  this.id = id;
  this.subject = topics[id];
  this.participants = [];
  this.Yes = [];
  this.No = [];
  this.Unsure = [];
}

// return the number of people who voted yes on a topic
Assembly.prototype.numYes = function(){
  return this.Yes.length;
}

// return the number of people who voted no on a topic
Assembly.prototype.numNo = function(){
  return this.No.length;
}

// return the number of people who were unsure about a topic
Assembly.prototype.numUnsure = function(){
  return this.Unsure.length;
}

// if the assembly has exceeded 6, which is greater, than the 2-2-2 structure
Assembly.prototype.atCapacity = function(){
  return this.participants.length >= 6;
}


/*
The removeUsersFromOtherAssemblies() function will iterate through the participants
of a newly balanced assembly and remove them other possible assemblies that were generated.
It is able to do so by examining the user map for a given user where the participant
was mapped to the assembly combinations where they were added.
*/
function removeUsersFromOtherAssemblies(mappingsUserToAssemblies,leftOver,finalAssembly){
  for(let participants = 0; participants < finalAssembly.participants.length; participants ++){
    let participant = finalAssembly.participants[participants];
    let participantMappingIndex = mappingsUserToAssemblies.findIndex(map => map.id == participant.Username);
    let participantMapping = mappingsUserToAssemblies[participantMappingIndex]; // mapping of user to assemblies
    let index = leftOver.findIndex(
      person  => person.Username === participantMapping.id);
      leftOver.splice(index,1);
      let assembliesOfUser = participantMapping[participantMapping.keyPairing]; // assemblies assigned to
      for(let i = 0; i < assembliesOfUser.length; i++){
        let assembly = assembliesOfUser[i];
        if(assembly != finalAssembly){ // need to remove from those assemblies
          let indexOfUser = assembly.participants.findIndex(
            participant  => participant.Username === participantMapping.id);
            assembly.participants.splice(indexOfUser,1);
            let indexOfPref = participantMapping.participant.preferences.findIndex(
              preference  => preference.topic == assembly.id );
              prefArray = assembly[participantMapping.participant.preferences[indexOfPref].response];
              let prefArrayRemove = prefArray.findIndex(
                user  => user.Username === participantMapping.id);
                prefArray.splice(prefArrayRemove,1);
              }
            }
          }
        }

/*
Returns index where assembly matches the topic (id) sent in
*/
function getMatchedAssemblyIndex(newAssemblies,id){
  let index = newAssemblies.findIndex(
    assembly  => assembly.id === id);
  return index;
}

/*
A map is generated for a given participant with the following fields,
id (string), participant (object), keyPairing (unique id), map of keypairing
which is an array of the assemblies the user has been added to
*/
function getParticipantMap(participant){
  let map = {};
  map.id = participant.Username;
  map.participant = participant;
  map.keyPairing = uniqid(participant);
  map[map.keyPairing] = []; // all combos of assemblies the user was added to
  return map;
}


/*
The generateAllPossibleAssemblies() function will generate all possible assemblies
for based on the users left that have not been assigned. It does so by iterating through
the users left over and  the "new assemblies" and either creating an assembly for each preference if that assembly
has not already been created or by adding an assembly (to "new assemblies") corresponding to that topic
if such assembly has been created. After the user is added to such an assembly, the
assembly will then be evaluated to test whether it meets the 2-2-2 structure. If so,
the assembly is balanced and is now added to the balancedAssemblies array. At this point,
the all the users in this assembly should be removed from the other possible assemblies
that they were added to as well.

At the end, we will now return all of the assemblies that we were able to balance.
*/
function generateAllPossibleAssemblies(balancedAssemblies,leftOver, newAssemblies,topics){
  let mappingsUserToAssemblies = []; // will contain a map that shows the assembly combos a given user was added to
  for(let i = leftOver.length - 1; i >= 0; i--){
    let participant = leftOver[i];
    let participantMapping = getParticipantMap(participant);
    for(let j = 0; j < participant.preferences.length; j ++){ // assign to assembly for each topic
      let preference = participant.preferences[j];
      let matchedAssemblyIndex = getMatchedAssemblyIndex(newAssemblies, preference.topic);
      if(matchedAssemblyIndex != -1){ // assembly found matching topic
        let assembly = newAssemblies[matchedAssemblyIndex];
        if(assembly[preference.response].length < 2){ // can add the user to that assembly
          assembly.participants.push(participant);
          assembly[preference.response].push(participant);
          participantMapping[participantMapping.keyPairing].push(assembly);
          mappingsUserToAssemblies.push(participantMapping);
        }
        if(assembly.atCapacity()){ // assembly now full since we added user
          balancedAssemblies.push(assembly);
          removeUsersFromOtherAssemblies(mappingsUserToAssemblies,leftOver,assembly); // remove from other combos
          newAssemblies.splice(matchedAssemblyIndex,1);
          break;
        }
      }else{ // assembly not found, so let's create one
        let newAssembly = new Assembly(preference.topic,topics); // make assembly to match user's first choice
        newAssembly[preference.response].push(participant);
        newAssembly.participants.push(participant);
        newAssemblies.push(newAssembly);
        participantMapping[participantMapping.keyPairing].push(newAssembly);
        mappingsUserToAssemblies.push(participantMapping);
      }
    }
  }
  return balancedAssemblies;
}

/*
Returns a helper function where all possible assemblies are considered where users
could be assigned.
*/
function finalAssemblyCleanUp(balancedAssemblies, leftOver,topics){
  return generateAllPossibleAssemblies(balancedAssemblies, leftOver, [], topics);
  //return allBalancedAssemblies(stillNeedAGroup,balancedAssemblies);
}

/*
The users in balancedAssemblies, all have been assigned to their first choice and
the assemblies are balanced. However, there are still some users where assemblies
were not able to be balanced for their first choice. From here, balancedTheRest() will generate this
list of users and try to find a combination where we can create assemblies that
fit the criteria of users.

*/
function balanceTheRest(balancedAssemblies,assemblies,topics){ // put everyone that needs pairing and pair together
  let stillNeedAGroup = [];
  for(let i = 0; i < assemblies.length; i++){
    let assembly = assemblies[i];
    for(let j = 0; j < assembly.participants.length; j++){
      stillNeedAGroup.push(assembly.participants[j]);
    }
  }
  return finalAssemblyCleanUp(balancedAssemblies,stillNeedAGroup,topics);
}

/*
2-2-1 structure, 1-2-1 structure, 1-1-1  structure is considered relatively balanced
*/
function isRelativelyBalanced(assembly){
  if(assembly.numYes() >= 1 && assembly.numNo() >=1 && assembly.numUnsure() >=1 ){
    return true
  }
  return false
}

/*
At this point, we have already removed the assemblies with a 2-2-2 and put into
the balancedAssemblies array. Now, we must balance the other assemblies. However,
their may be situations where the conversation can already be substantive where the
assembly may not necessarily be balanced such as a 2-2-1, 1-2-1, or even a -1-1-1 (because
remember at this point, the assemblies generated are still each user's first choice).
The rationale on why I believe a 1-1-1 could be relatively balanced will be explained
further in the SOLUTION.md file.

If an assembly does meet this relativelyBalanced criteria, then we will add it to the
balancedAssemblies array.
*/
function getBalancedAssemblies(balancedAssemblies,assemblies,topics){
  for(let i = assemblies.length - 1; i >= 0; i --){
    let assembly = assemblies[i];
    if(isRelativelyBalanced(assembly)){ // meets criteria noted above to be relatively balanced
      balancedAssemblies.push(assembly);
      assemblies.splice(i,1);
    }
  }
  return balanceTheRest(balancedAssemblies, assemblies,topics); // balance remaining assemblies
}


/*
The balanceAssemblies fucntion will create an array where the balanced assemblies
will be held. For this part of the program, we are only considered an assembly
balanced if it contains the 2-2-2 structure. If an assembly already has such structure
then we will consider the assembly balanced. We will then call getBalancedAssemblies
to generate balanced assemblies based on which assemblies are left following the
removal of ones with the 2-2-2 structure.
*/
function balanceAssemblies(assemblies,topics){ // this function will look at the assemblies and try to balance them
  let balancedAssemblies = [];
  for(let i = assemblies.length -1; i >= 0; i --){ // loop backwards because we are removing items
    let assembly = assemblies[i];
    if(assembly.atCapacity()){ // at this point, only assemblies with 2-2-2 structure
      balancedAssemblies.push(assembly);
      assemblies.splice(i,1);
    }
  }
  assemblies.sort(function(a,b){
    return b.participants.length - a.participants.length;
  })
  //return balancedAssembliesTest(balancedAssemblies,assemblies);
  return getBalancedAssemblies(balancedAssemblies,assemblies,topics);
}

/*
The assignToAssembly function will assign a user to their first preference. If an
assembly does not already exist for that given preference, then one will be created where
the user will be assigned.
*/
function assignToAssembly(assemblies, user, topics){
  let pref = user.preferences[0];
  let prefTopic = pref.topic;
  for(let i = 0; i < assemblies.length; i ++){
    let assembly = assemblies[i];
    if(assembly.id == prefTopic && assembly[pref.response].length < 2){ // found user's first choice
      assembly[pref.response].push(user);
      assembly.participants.push(user);
      return;
    }
  }
  let newAssembly = new Assembly(prefTopic,topics); // make assembly to match user's first choice
  newAssembly[pref.response].push(user);
  newAssembly.participants.push(user);
  assemblies.push(newAssembly);
  return;
}

/*
The createAssemblies function initially assigns users to assemblies based
on their preference for a given topic. *The preferences array is sorted in
order where their higher priority preferences come first*. Then the function will
balance the assemblies and return this result.
*/
function createAssemblies(usersArray,topics){
  let assemblies = [];
  for(let i = 0; i < usersArray.length; i++){
    let user = usersArray[i];
    assignToAssembly(assemblies,user,topics);
  }
  return balanceAssemblies(assemblies,topics); // balance the assemblies
}

/*
  Returns the assemblies, created by the helper function createAssemblies
*/
function getAssemblies(usersArray,topics){
  return createAssemblies(usersArray,topics);
}

/*
The getTopicMap function will create a map that matches the topic number to the
subject of the topic and will return this map.
*/
function getTopicMap(topics){ // id mapped to topic
  let map = {};
  for(let i= 1; i < topics.length; i++){
    topic = topics[i];
    map[parseInt(topic[0])] = topic[1];
  }
  return map;
}

/*
The getUserObjs function turns the participant data that was read in, into
objects. The user object will contain these fields:
name (string), username(string), preferences(array of objects), where the objects
correspond to a users preference. The preference object will contain the topic to which
it belongs, a user's response,viewpoint, reception to a given topic. An overallRating will then be computed where
more weight is given to the reception of a topic than the viewpoint. My rationale for such
is explained in the SOLUTION.md file.
*/
function getUserObjs(dataParsed){ // turns the arrays into objects
  let arrayOfObjs = [];
  for(let i = 1; i < dataParsed.length; i++){
    let headers = dataParsed[0];
    let user = dataParsed[i];
    let map = {};
    map.preferences = [];
    topic = 1 // will loop through topics by iterating through user
    for(let j = 1; j < user.length; j ++){
    if(j < 3) map[headers[j]] = user[j];
    else if(j % 3 == 0){ // a user's "preference" for a given topic
      let preference = {};
      let viewpoint = user[j+1];
      let receptive = user[j+2];
      preference.response = user[j];
      preference.viewpoint = viewpoint;
      preference.receptive = receptive;
      preference.overallRating = Math.round((.6*receptive + .4*viewpoint)*100)/100; // more weight given to reception
      preference.topic = topic.toString();
      map.preferences.push(preference);
      topic++;
      }
    }
    map.preferences.sort(function(a,b){ // sort this array high to low
      return b.overallRating - a.overallRating;
    }); // user's highest preference comes first
    arrayOfObjs.push(map);
  }
  return arrayOfObjs;
}


/*
The readData function reads in the participant and topic data and will add this
data to the local arrays. Once all of the data has been read into system, the
callback function will be called. The callback function will begin the process of
turning this data into objects and finding assemblies based on the topics.
*/
function readData(callback){ // reads in the csv file
  let dataFiles = []; // will hold participant data
  fs.createReadStream('./data/participants.csv')
    .pipe(csv())
    .on('data', function(data){
      dataFiles.push(data);
    })
    .on('end', function(data){
      console.log("finished reading participtants");
      let topics = [];
      fs.createReadStream('./data/topics.csv')
        .pipe(csv())
        .on('data', function(data){
          topics.push(data);
        })
        .on('end', function(data){
          console.log("finished readings topics");
          callback(dataFiles,topics);
        });
    });
}

// outputs the assembly to a json file
function outputToJSON(assemblies){
  let data = [];
  for(let i = 0; i < assemblies.length; i++){
    let assembly = assemblies[i];
    let map = {};
    map.id = assembly.id;
    map.subject = assembly.subject;
    map.participants = JSON.stringify(assembly.participants);
    map = JSON.stringify(map);
    data.push(map);
  }
  data = JSON.stringify(data);
  fs.writeFile('assembly_placement.json',data,'utf8',function(err){
    if(err) console.log("error");
  });
}

/*
The main function will read in the user and topic data and turn these into
objects. Assemblies will then be generated based on the user's experience and
feelings toward certain prompts. Finally, the assemblies will be output into a
json file.
*/
async function main(){
  readData(function(dataFiles,topics){
    let arrayOfUsers = getUserObjs(dataFiles);
    let mapOfTopics = getTopicMap(topics);
    let assemblies = getAssemblies(arrayOfUsers,mapOfTopics);
    outputToJSON(assemblies);
    //console.log(assemblies);
  });

}

main()
