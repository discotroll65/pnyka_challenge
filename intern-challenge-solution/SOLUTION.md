# Solution Explanation

Background
-----------
To determine the best algorithm to use for the program, I first had to make some
assumptions as to what would be best for the user as a platform. I believe that
the goal of a system such as this is to get the conversation started on difficult topics.
The best way to do so is to is to try to assign users to assemblies where they
1) feel the most comfortable to express their opinion and 2) will respect
the opinions of others.

 # 2 can be easily collected through the topic 'receptiveness'
 # 1 however, is a combination of both the user's knowledge of a topic and
 the 'receptiveness' of other participants in a given assembly

 So essentially, a pleasant solution for the system is one where a given user
 is assigned to an assembly where they are receptive to feedback from other users,
 the other users are receptive to their[said user's] feedback, and they are knowledgeable about a given
 topic.

 To get started, for each user I had to determine which topics would be most suitable
 for them. The solution I chose for this was to capture an overallRating for a given
 user's stance on a topic based on how receptive the user would be and their knowledge
 of the topic. I gave more weight (60%) to receptiveness than to knowledge (40%) because
 I felt that receptiveness to feedback is more of a contributor to a good conversation
 than just knowledge. However, knowledge is still somewhat important so the user
 feels comfortable enough to discuss certain points about the topic. (Assumption:
  A conversation where a user may be less knowledgable about a subject but very receptive is more
  pleasant and comfortable to others than a conversation where a user may be more
  knowledgable and less receptive).

 At this point, I was able to determine a numerical fit (.6 * receptiveness + .4 * knowledge) for a given user for each assembly topic. An array was made and sorted in decreasing order where
 the best topic fit came first.

 Now that the background has been discussed, I will show the steps for my algorithm

 1. Try to assign each user to their "first choice" assembly (assembly where best fit)

 2. At this point, some assemblies were balanced (2-2-2). These assemblies were fine
 and added to an array of balanced assemblies. However, even though some were balanced,
 there were still others that weren't.

 3. Iterate through the "unbalanced assemblies" and determine which ones were relatively
 balanced. I considered relatively balanced to be combinations of 2-2-1, 1-1-2, or even
 1-1-1. Reason: Because think about it, the users in these "unbalanced" assemblies at this point
 are still in the assemblies associated with their best fit and where the participants are
 most comfortable. Even though they are unbalanced, there is still room for a substantive
 conversation because the users assigned are comfortable discussing the topic. So instead
 of trying to assign these users to other assemblies where they may not be comfortable or
 trying to assign other users to these assemblies where these users could potentially harm the
 comfortability rating of the assembly, it is best for these "relatively" balanced
 assemblies to be considered "balanced". As such, assemblies that met this criteria were
 pushed to the balanced assemblies array.

 4. Users in assemblies (not balanced or relatively balanced) that didn't meet the criteria pushed into array. From here,it became essential to try to maximize the number of users from this list into assemblies
even if the assembly they will be assigned to won't be their first choice. The solution here was
to make an array of all possible assemblies. (Premise: At these point, we haven't been able
to assign users to an assembly of first choice, but we can still try to make the experience
great by trying to assign to a balanced assembly of another topic where there is the chance
this user may still be somewhat knowledgable and receptive about)

5. To accommodate #4, the solution is to get an array of all of the users that aren't in balanced (or relatively balanced) assemblies and scratch all of the unbalanced assemblies made previously and begin to essentially try to make new ones. To do so, the process was to iterate through these users' preferences and
either make a new assembly if one didn't exist for each topic or to assign the user
to one of the new assemblies made. As stated, this is done for each topic. So this essentially
becomes a mechanism to generate all possible assemblies.

6. Once a given assembly out of these new assemblies becomes balanced, add the assembly
to the balanced assembly array and remove the users in that assembly from the other possible
assemblies they may have been added to.

7. At this point, the state of the balanced assemblies is this: assemblies where
the users were able to get their first choice (balanced and relatively balanced) and assemblies where there were enough users to be considered balanced (even though it may not be everyone's first choice
but at least everyone's stances are accommodated). Most users (82%) are now
assigned.

8. There now becomes the choice of trying to assign the remaining 18% to assemblies already
created or even possibly just make assemblies that don't accommodate the users topic
responses and preferences. Or another route is to let the users remain unassigned for this round, and
to give them preferential treatment for the next go round of topic assignments.

* I believe that the latter approach is better because by trying to force the users into
other assemblies, you may ruin the experience of other users by making assemblies too large
or even ruin the experience of the unassigned users if you try to add them to a mix-matched
group. *  

Feel free to let me know if you have any questions about any of my premises or assumptions.
