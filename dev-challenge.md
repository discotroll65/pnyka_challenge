# Pnyka Development Challenge  
  

For questions, comments and submissions, please email [grey@pnyka.co](mailto:grey@pnyka.co)

## Overview  
  
Thank you for your interest in joining the Pnyka Engineering team!  
  
At Pnyka, we first and foremost are looking for intellectually curious people who can relate to and understand several different viewpoints. That’s not enough by itself, however. Everyone at Pnyka Engineering is expected to USE those viewpoints to make educated decisions about how to plan, implement, and test technical solutions.  
  
You may be thinking “this sounds obvious”… and given our mission it should be. The problem is that it’s way easier to consider multiple viewpoints than it is to actually use them in a productive manner. What happens when two viewpoints are at odds with each other? What happens when you don’t have access to views different from your own? What if the viewpoints you’re considering are too vague or high-level for practical use? What is the best method to utilize several viewpoints to make a single decision? And how do you recognize when your own biases may be clouding your decision making?  
  
With that in mind, we have assembled a technical challenge for interested candidates that not only encompasses our mission and values, but gives candidates a glimpse into the type of challenges that they can expect to encounter on the job. This includes elements of agile process, putting clearly defined plans in place to guide implementation, implementation itself, and overall communication.  
  
This challenge is multi-faceted, ambiguous, demanding, open-ended and focused on practical coding. There is no exact right or wrong answer and is meant only to give you an opportunity to showcase your technical abilities.  
  
## Instructions  
  
Due to the nature of this challenge, we ask that you spend about five hours (and absolutely no more than ten hours) on it. It is perfectly acceptable to include “TODOs” in your submission for pieces you didn’t quite get to, but have a plan for.

  

### Prompt  
  
Imagine you’ve just finished your onboarding at Pnyka and are given your first big project. You’ve been given a previous intern’s code for placing users into assemblies and are told to use it to create an ‘administration portal’ that any ‘assembly leader’ can use to manage the user placements into assemblies as desired. This should be completed as a ReactJS (let us know if using ReactJS is a problem) front-end UI that can display and interact with the data contained within the submission.

  

You have also been given two resources to complete this challenge. First, you have an intern-level coding task complete with description of the problem and data to use for completion. Use this to get a better understanding of the data you’ll be working with. Second, you have one example of a completed solution. Use this to power the underlying data of our front end UI.

  

Your goal is to read and understand the submission to the intern’s project and build the aforementioned ‘administration portal’ focusing on users’ mobile experience (web browsing on mobile devices).

  

Naturally, you begin by asking about the acceptance criteria and exact specifications for the administration portal. Thankfully, there are only a few deliverables that are absolutely required:

  
### Deliverables

  

#### Acceptance Criteria

As an ‘assembly leader’,

1.  I can see a list of user responses complete with all the associated data.
    
2.  I can click a button to run the matching script and display the assemblies that are to be created, topic for each.
    

1.  I can view a list of users placed in each assembly with their response for that assembly.
    

4.  I can drag and drop users from one assembly to another
    

1.  I notice that the response for a given topic is updated as they are placed into different assemblies
    

6.  After I drag users into the proper assemblies, I can click another button to save/download a file that contains the assemblies and their associated users.
    

  

#### Notes

Make sure to provide detailed instructions on how a developer could take your code and get it running locally to verify the above criteria were met.

  

If anything is clear please reach out to us with questions. We will do our best to answer them ASAP.

  

### Helpful Additions (optional, only if time permits)

  

Create a short bulleted list of tools, tricks, libraries and references used - what does your development environment and process consist of? Put this in a text file called DEV_PROCESS at the top level of the project

  

Keep notes on things you’ve learned during this process. Put this in a text file called LEARNINGS or as a separate section of DEV_PROCESS

  

As you begin running and understanding the intern’s code, record bugs as you find them including:

1. Steps to reproduce

2. Potential impact the bug has on the user/business/engineers

3. A potential solution

4. Put this in a text file called BUGS at the top level of the project

  

Provide instructions on how you would deploy this web app for use on the world wide web. This can be anything between be high level thoughts and detailed step-by-step instructions. Put this in a text file called DEPLOYMENT at the top level of the project

  
## What we're looking for

1. How you reason through technical problems.

2. How you read code and understand it.

3. How you write code and test it.

4. How you use tools and libraries to make your life easier

5. How you communicate as you do the above.

  
### Tips  
1. Show your work and make sure to put a spotlight on what you’re most proud of.

2. Showcase your strengths.

3. Approach this as a learning opportunity.

4. Take notes (ideally annotating code) as you learn/understand.

5. DO NOT spend lots of time figuring out how to fix small issues that block progress, instead make a note of them and explain how you might go about learning how to fix it.

6. Be sure to provide reasoning for decisions, but especially for ones where you had to make a tough choice.

  
## Submission Checklist  
1. Pull request with your code and text files

2. [Optional] DEV_PROCESS text file

3. [Optional] LEARNINGS text file

4. [Optional] BUGS text file

5. [Optional] DEPLOYMENT text file

6. [Optional] NOTES text file
